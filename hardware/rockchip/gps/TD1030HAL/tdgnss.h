#ifndef __TDGNSS_H__
#define __TDGNSS_H__

#include <hardware/gps.h>
#include <cutils/log.h>
#define  TD_LOG_TAG  "TD1030DBG"


#define  D(...)           ALOGD(__VA_ARGS__)

#define DEFAULT_BDPRNADD         200
#define DEFAULT_UART_BAUD_RATE     115200
#define DEFAULT_GNSS_MODE        3
#define DEFAULT_UART_DRIVER     "/dev/ttyS0"

#define CMD_BD_COLD         "$CCSIR,1,1*48\r\n"
#define CMD_GPS_COLD        "$CCSIR,2,1*4B\r\n"
#define CMD_BD_GP_COLD      "$CCSIR,3,1*4A\r\n"
#define CMD_GL_COLD         "$CCSIR,4,1*4D\r\n"
#define CMD_BD_GL_COLD      "$CCSIR,5,1*4C\r\n"
#define CMD_GP_GL_COLD      "$CCSIR,6,1*4F\r\n"

#define CMD_BD_HOT          "$CCSIR,1,3*4A\r\n"
#define CMD_GPS_HOT         "$CCSIR,2,3*49\r\n"
#define CMD_BD_GP_HOT       "$CCSIR,3,3*48\r\n"
#define CMD_GL_HOT          "$CCSIR,4,3*4F\r\n"
#define CMD_BD_GL_HOT       "$CCSIR,5,3*4E\r\n"
#define CMD_GP_GL_HOT       "$CCSIR,6,3*4D\r\n"

#define  SET_CMD_TDMODULE_LEN      15

#define TD_CONFIGFILE_PATH  "/vendor/etc/tdgnss.conf"
#define TD_AGPSFILE_PATH  "/storage/.agpsdata"

#define TD_CONFIGFILE_NUM              25

#define TD_OFF      0
#define TD_ON       1
//#define TRUE        1
//#define FALSE       0

struct configfile_item
{
    char *name;
    char *value;
};

enum
{
    MODE_OFF = 0,
    MODE_GPS = 1,
    MODE_BD = 2,
    MODE_BD_GPS = 3,
    MODE_GL = 4,
    MODE_BD_GL = 5,
    MODE_GP_GL = 6,
};

enum
{
    CMD_QUIT  = 0,
    CMD_START = 1,
    CMD_STOP  = 2,
    CMD_ENABLE_GPS,
    CMD_ENABLE_BD,
    CMD_ENABLE_GL,
    CMD_ENABLE_BD_GP,
    CMD_ENABLE_GP_GL,
    CMD_ENABLE_BD_GL,
    CMD_START_AGPS,
    CMD_AGPS,
    BD_GP_COLD,
    GP_GL_COLD,
    BD_GL_COLD,
    BD_COLD,
    GPS_COLD,
    GL_COLD,
    CMD_UPDATE_TD,
    CMD_SENT_TO_CHIP,
};

enum nmea_state
{
    NMEA_OFF = 0,
    NMEA_BDRMC,
    NMEA_GPRMC,
    NMEA_GNRMC,
    NMEA_GLRMC,
    NMEA_1ST_GNGSA,
    NMEA_2ND_GNGSA,
    NMEA_OTHER,
};

enum debug_mask_bit
{
  D_TOKEN = 0,
  D_GSV,
  D_GSA,
  D_NMEA,
  D_FIX,
  D_FUCTION,
  D_CONFIGFILE,
};

typedef struct
{
    int                     init;
    int                     fd;
    GpsCallbacks            callbacks;
    GpsStatus               status;
    pthread_t               thread;
    int                     control[2];
} GpsState;

typedef struct
{
    int                     agps_mode;
    int                     nv_mode;
    int                        work;
    int                     on_need;
} TDMODE;

struct __updatefileinfo{
    unsigned char      updatefiletype;
};


struct __strconfigfile{
    char projectname[100];
    char uartpath[100];
};
typedef struct _TD1030Config
{
    struct __strconfigfile strconfigfile;
    struct __updatefileinfo updatefileinfo;
    int gnss_mode;
    int fast_fix;
    int agnss_enable;
    int uartboadrate;
    int bdprnaddr;
    int toallprn;
    unsigned char power_flag;
    unsigned char recvgpgsv;
    unsigned char recvgpgsa;
    unsigned int debugmask;
} TD1030Config;

struct utc_t{    
    int year;    
    int month;    
    int day;    
    int hour;    
    int min;    
    int sec;
};

int TdUartOpen(void);
void SetApUartbaud(int baud);

extern TD1030Config tdconfig;
unsigned int GetDebugMaskBit(enum debug_mask_bit bit);
void Creat_update_thread(void);
void SendResultToApp(int res);

#endif
