# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# We're moving the emulator-specific platform libs to
# development.git/tools/emulator/. The following test is to ensure
# smooth builds even if the tree contains both versions.
#

#SYSARCH = 32
SYSARCH = 64

LOCAL_PATH := $(call my-dir)

# HAL module implemenation stored in
# hw/<GPS_HARDWARE_MODULE_ID>.<ro.hardware>.so
include $(CLEAR_VARS)

ifeq ($(SYSARCH),64)
LOCAL_MODULE_RELATIVE_PATH := hw
else
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
endif

LOCAL_CFLAGS += -Wunused
LOCAL_CFLAGS += -Wall
LOCAL_PRELINK_MODULE := false
LOCAL_SHARED_LIBRARIES := liblog libcutils libhardware
SRC_FILES_LIST := $(wildcard $(LOCAL_PATH)/*.c)
SRC_FILES_LIST += $(wildcard $(LOCAL_PATH)/asn-rrlp/*.c)
SRC_FILES_LIST += $(wildcard $(LOCAL_PATH)/asn-supl/*.c)
LOCAL_SRC_FILES := $(SRC_FILES_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES += external/openssl/include/ 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/asn-rrlp $(LOCAL_PATH)/asn-supl
LOCAL_SHARED_LIBRARIES += libcrypto
LOCAL_SHARED_LIBRARIES += libssl

LOCAL_MODULE := gps.default

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

################ AGNSS/libtdctypto.so to /system/lib/ ############
#include $(CLEAR_VARS)
#LOCAL_MODULE := libtdcrypto.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
#ifeq ($(SYSARCH),64)
#LOCAL_SRC_FILES := AGNSS/64/libtdcrypto.so
#else
#LOCAL_SRC_FILES := AGNSS/32/libtdcrypto.so
#endif
#include $(BUILD_PREBUILT)
#
################ AGNSS/libtdssl.so to /system/lib/ ############
#include $(CLEAR_VARS)
#LOCAL_MODULE := libtdssl.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
#ifeq ($(SYSARCH),64)
#LOCAL_SRC_FILES := AGNSS/64/libtdssl.so
#else
#LOCAL_SRC_FILES := AGNSS/32/libtdssl.so
#endif
#include $(BUILD_PREBUILT)
#
################ AGNSS/libtdsupl.so /system/lib/ ############
#include $(CLEAR_VARS)
#LOCAL_MODULE := libtdsupl.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
#ifeq ($(SYSARCH),64)
#LOCAL_SRC_FILES := AGNSS/64/libtdsupl.so
#else
#LOCAL_SRC_FILES := AGNSS/32/libtdsupl.so
#endif
#include $(BUILD_PREBUILT)
#
################ AGNSS/supl-client /system/bin/ ############
#include $(CLEAR_VARS)
#LOCAL_MODULE := supl-client
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_CLASS := EXECUTABLES
#LOCAL_MODULE_PATH := $(TARGET_OUT_EXECTUABLES)
#ifeq ($(SYSARCH),64)
#LOCAL_SRC_FILES := AGNSS/64/supl-client
#else
#LOCAL_SRC_FILES := AGNSS/32/supl-client
#endif
#include $(BUILD_PREBUILT)
#
